﻿using Emgu.CV;
using Emgu.CV.Structure;
using MahApps.Metro.Controls;
using Microsoft.Win32;
using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.ViewModels;
using APO_Lab_App.Classes.Models;
using System.IO;
using APO_Lab_App.Classes.Histogram;
using APO_Lab_App.Classes;
using PointConverter = APO_Lab_App.Classes.Converter.PointConverter;
using APO_Lab_App.Classes.Events;
using APO_Lab_App.Classes.ImageProcessing;
using APO_Lab_App.Classes.Managers;
using APO_Lab_App.Classes.TabImageDataSpace;
using System.Globalization;
using System.Reflection;

namespace APO_Lab_App
{
    /// <summary>
    /// Main class with main window.
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        /// <summary>
        /// Reference to this window.
        /// </summary>
        public static MainWindow Instance { get; private set; }

        /// <summary>
        /// Reference to view model class with pages.
        /// </summary>
        public TabPageViewModel TabPageViewModel { get; private set; }

        /// <summary>
        /// Reference to Windows manager.
        /// </summary>
        public WindowsManager WindowsManager { get; private set; }  

        /// <summary>
        /// Current language code. Example: "pl-PL", "en-US"
        /// </summary>
        public string CurrentLanguageCode { get; set; }

        private List<System.Drawing.Point> clickedPointsOnImage;

        private string lastLoadedImagePath;

        private int currentPageID = 0;

        private bool secondClickProfileLine;

        public MainWindow()
        {
            Instance = this;

            InitializeComponent();
     
            clickedPointsOnImage = new List<System.Drawing.Point>();
            WindowsManager = new WindowsManager();
            TabPageViewModel = new TabPageViewModel();
            TcImagePages.DataContext = TabPageViewModel;
        }

        /// <summary>
        /// Change app language.
        /// </summary>
        /// <param name="languageCode">Language code, example: "en-US", "pl-PL"</param>
        public void ChangeLanguage(string languageCode)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageCode);
            Properties.Settings.Default.CurrentLanguageCode = CurrentLanguageCode;
            Properties.Settings.Default.Save();

            MessageBox.Show("Language changed, please restart APP to show changes.", "Changed language", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public void PrepareViewTabAfterImageProcessing(Bitmap imageBitmap, string operationName) => CreateNewTabAfterImageProcessing(imageBitmap, operationName);

        /// <summary>
        /// Load image from file using OpenFileDialog and show in Image control.
        /// </summary>
        /// <returns></returns>
        public Image<Bgr, Byte> LoadImageFromFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "All Images Files (*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif)|*.png;*.jpeg;*.gif;*.jpg;*.bmp;*.tiff;*.tif" +
            "|PNG Portable Network Graphics (*.png)|*.png" +
            "|JPEG File Interchange Format (*.jpg *.jpeg *jfif)|*.jpg;*.jpeg;*.jfif" +
            "|BMP Windows Bitmap (*.bmp)|*.bmp" +
            "|TIF Tagged Imaged File Format (*.tif *.tiff)|*.tif;*.tiff" +
            "|GIF Graphics Interchange Format (*.gif)|*.gif";
            
            if (openFileDialog.ShowDialog() == true)
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(openFileDialog.FileName);
                bitmap.EndInit();

                lastLoadedImagePath = openFileDialog.FileName;

                return new Image<Bgr, Byte>(openFileDialog.FileName);
            }

            return null;
        }

        /// <summary>
        /// Get gray Image from selected tab. Only for gray Image!
        /// </summary>
        /// <returns>Image<Gray, Byte> image</Bgr></returns>
        public Image<Gray, Byte> GetGrayImageFromSelectedTab()
        {
            TabImageDataGray tabImageDataGray = (TabImageDataGray)TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].TabImageData;
            return tabImageDataGray.GrayImage;
        }

        /// <summary>
        /// Save selected image to file.
        /// </summary>
        private void SaveImageToFile()
        {
            if (TcImagePages.SelectedIndex == -1) return;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image file (*.bmp)|*.bmp";

            if (saveFileDialog.ShowDialog() == true)
            {
                var encoder = new BmpBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)BitmapConverter.BitmapToBitmapImageConvert(GetImageFromSelectedTab())));
                using (FileStream stream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    encoder.Save(stream);
            }
        }

        /// <summary>
        /// Make duplicate selected image.
        /// </summary>
        private void DuplicateImage()
        {
            if (TcImagePages.SelectedIndex == -1) return;

            PrepareViewTabAfterImageProcessing(GetImageFromSelectedTab(), "_duplicate");
        }

        /// <summary>
        /// Creates a new page in the tab controller as a new item is added to the list after the image has been successfully loaded.
        /// </summary>
        /// <param name="grayscaleImage">Set true if you want convert image to grayscale, false is default.</param>
        private void CreateNewTabAfterLoadImage(bool grayscaleImage = false)
        {
            Image<Bgr, Byte> loadedImageFromFile = LoadImageFromFile();

            TabImageData tabImageData = null;

            if (loadedImageFromFile == null) return;

            if (grayscaleImage)
            {
                tabImageData = new TabImageDataGray(loadedImageFromFile, lastLoadedImagePath);
                TabPageViewModel.TabPageDatas.Add(new TabPageDataModel(currentPageID, tabImageData, new HistogramGrayscale(tabImageData.Bitmap), Path.GetFileNameWithoutExtension(tabImageData.ImagePath)));
            }
                
            else
            {
                tabImageData = new TabImageDataRGB(loadedImageFromFile, lastLoadedImagePath);
                TabPageViewModel.TabPageDatas.Add(new TabPageDataModel(currentPageID, tabImageData, new HistogramRGB(tabImageData.Bitmap), Path.GetFileNameWithoutExtension(tabImageData.ImagePath)));
            }

            currentPageID++;
        }

        /// <summary>
        /// Get RGB Image from selected tab. Only for RGB Image!
        /// </summary>
        /// <returns>Image<Bgr, Byte> image</Bgr></returns>
        private Image<Bgr, Byte> GetRGBImageFromSelectedTab()
        {
            if (TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].GrayImage) return null;

            TabImageDataRGB tabImageDataRGB = (TabImageDataRGB)TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].TabImageData;
            return tabImageDataRGB.RGBImage;
        }
        
        /// <summary>
        /// Check image from current selected index is grayscale.
        /// </summary>
        /// <returns>True if image is grayscale, else no.</returns>
        private bool IsGrayImageFromSelectedTab() => TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].GrayImage;

        /// <summary>
        /// Get image from current selected index.
        /// </summary>
        /// <returns>Bitmap image.</returns>
        private Bitmap GetImageFromSelectedTab() => TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].TabImageData.Bitmap;

        /// <summary>
        /// Get gray histogram object from current selected index.
        /// </summary>
        /// <returns>HistogramGrayscale</returns>
        private HistogramGrayscale GetGrayHistogramFromSelectedTab() => TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].Histogram as HistogramGrayscale;

        /// <summary>
        /// Get RGB histogram object from current selected index.
        /// </summary>
        /// <returns>HistogramRGB</returns>
        private HistogramRGB GetRGBHistogramFromSelectedTab() => TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].Histogram as HistogramRGB;


        /// <summary>
        /// Create a new tab in APP with result image after image processing.
        /// </summary>
        /// <param name="bitmapAfterProcessing">Bitmap after processing</param>
        /// <param name="operationName">Image processing name</param>
        private void CreateNewTabAfterImageProcessing(Bitmap bitmapAfterProcessing, string operationName)
        {
            TabImageDataGray tabImageDataGray = new TabImageDataGray(bitmapAfterProcessing.ToImage<Gray, Byte>());
            TabPageViewModel.TabPageDatas.Add(new TabPageDataModel(currentPageID, tabImageDataGray, new HistogramGrayscale(bitmapAfterProcessing), Path.GetFileNameWithoutExtension(tabImageDataGray.ImagePath) + operationName));

            currentPageID++;
        }

        /// <summary>
        /// Get datas between two points on image, needed for draw profile line.
        /// </summary>
        private void GetDataBetweenTwoPoints()
        {
            LineIterator lineIterator = new LineIterator(GetGrayImageFromSelectedTab().Mat, clickedPointsOnImage[0], clickedPointsOnImage[1]);
            int count = lineIterator.Count;
            List<System.Drawing.Point> points = new List<System.Drawing.Point>();
            List<byte[]> pixels = new List<byte[]>();
            List<int> pixelsInt = new List<int>();

            for (int i = 0; i < count; ++i)
            {
                points.Add(lineIterator.Pos);
                byte[] data = lineIterator.Data as byte[];
                pixels.Add(data);
                pixelsInt.Add(data[0]);

                for (int j = 0; j < data.Length; ++j)
                {
                    data[j] = (byte)(255 - data[j]);
                }

                lineIterator.Data = data;
                lineIterator.MoveNext();
            }

            SelectedPointsOnImage.SelectedPointsOnImageInvoke(typeof(MainWindow), TabPageViewModel.TabPageDatas[TcImagePages.SelectedIndex].PageID, points, pixelsInt);
        }


        #region Controls events
        private void LoadGrayImage_Click(object sender, RoutedEventArgs e) => CreateNewTabAfterLoadImage(true);
        private void LoadColorImage_Click(object sender, RoutedEventArgs e) => CreateNewTabAfterLoadImage();
 
        private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!IsGrayImageFromSelectedTab()) return;

            if (clickedPointsOnImage.Count == 2)
            {
                secondClickProfileLine = false;
                clickedPointsOnImage.Clear();
            }

            clickedPointsOnImage.Add(PointConverter.WindowsPointToDrawingPoint(e.GetPosition((IInputElement)sender)));

            if (secondClickProfileLine) GetDataBetweenTwoPoints();

            secondClickProfileLine = !secondClickProfileLine;
        }

        private void EqualizeHistogram_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.EqualizeHistogram(GetGrayImageFromSelectedTab().ToBitmap(), GetGrayHistogramFromSelectedTab().HistogramGrayscaleData), "_equalized");
        private void StretchHistogram_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.StretchHistogram(GetGrayImageFromSelectedTab().ToBitmap()), "_stretch");
        private void Negation_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Negation(GetGrayImageFromSelectedTab().ToBitmap()), "_negation");
        private void Threshold_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_THRESHOLD_WINDOW_KEY, true);
        private void ThresholdGrayLevel_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_THRESHOLD_GRAY_LEVEL_WINDOW_KEY, true);
        private void Posterize_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_POSTERIZE_WINDOW_KEY, true);
        private void StretchRange_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_STRETCH_RANGE_WINDOW_KEY, true);
        private void LinearSmoothingBlur_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.LinearSmoothingBlur(GetGrayImageFromSelectedTab().ToBitmap()), "_linearSmooth");
        private void LinearSmoothingGaussBlur_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.LinearSmoothingGaussBlur(GetGrayImageFromSelectedTab().ToBitmap()), "_gauss");
        private void EdgeDetectionSobel_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.EdgeDetectionSobel(GetGrayImageFromSelectedTab().ToBitmap()), "_sobel");
        private void EdgeDetectionLaplacian_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.EdgeDetectionLaplacian(GetGrayImageFromSelectedTab().ToBitmap()), "_laplacian");
        private void EdgeDetectionCanny_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.EdgeDetectionCanny(GetGrayImageFromSelectedTab().ToBitmap()), "_canny");
        private void EdgeDetectionPrewitt_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.EdgeDetectionPrewitt(GetGrayImageFromSelectedTab().ToBitmap()), "_prewitt");
        private void LinearSharp_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.LinearSharp(GetGrayImageFromSelectedTab().ToBitmap()), "_lineSharp");
        private void UniversalNeighLinearOperation_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_UNIVERSAL_LINEAR_OPERATIONS_WINDOW, true);
        private void MedianFilter_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_MEDIAN_FILTER_WINDOW, true);
        private void AddImages_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_ADD_IMAGES_WINDOW, true);
        private void Blending_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_BLENDING_WINDOW, true);
        private void LogicOperations_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_LOGIC_OPERATIONS_WINDOW, true);
        private void Erode_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Erode(GetGrayImageFromSelectedTab().ToBitmap()), "_erode");
        private void Dilate_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Dilate(GetGrayImageFromSelectedTab().ToBitmap()), "_dilate");
        private void OpenMorph_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.OpenMorph(GetGrayImageFromSelectedTab().ToBitmap()), "_openMorph");
        private void CloseMorph_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.CloseMorph(GetGrayImageFromSelectedTab().ToBitmap()), "_closeMorphh");
        private void FiltrOneStageSmooth_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.FiltrOneStageSmooth(GetGrayImageFromSelectedTab().ToBitmap()), "_filterOneSmooth");
        private void FiltrOneStageSharp_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.FiltrOneStageSharp(GetGrayImageFromSelectedTab().ToBitmap()), "_filterOneSharp");
        private void FiltrTwoStages_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_FILTER_TWO_STAGERS_WINDOW, true);
        private void Adaptive_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Adaptive(GetGrayImageFromSelectedTab().ToBitmap()), "_adaptive");
        private void Otsu_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Otsu(GetGrayImageFromSelectedTab().ToBitmap()), "_otsu");
        private void Watershed_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Watershed(GetGrayImageFromSelectedTab().ToBitmap()), "_watershed");
        private void PlotLine_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_PLOT_PROFILE_WINDOW_KEY, false);
        private void Skeletalisation_Click(object sender, RoutedEventArgs e) => PrepareViewTabAfterImageProcessing(ImageProcessing.Skeletalisation(GetGrayImageFromSelectedTab().ToBitmap()), "_skelet");


        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e) => Application.Current.Shutdown();
        private void ExitApp_Click(object sender, RoutedEventArgs e) => Application.Current.Shutdown();

        private void AboutApp_Click(object sender, RoutedEventArgs e) => MessageBox.Show("Aplikacja wykonana w ramach zajęć laboratoryjnych - Algorytmy przetwarzania obrazów. \n Autor: Mikołaj Wysocki \n Prowadzący: mgr inż. Łukasz Roszkowiak \n WIT grupa ID: ID06IO2 \n Wersja programu: " + Assembly.GetExecutingAssembly().GetName().Version.Major + "." + Assembly.GetExecutingAssembly().GetName().Version.Minor + "." + Assembly.GetExecutingAssembly().GetName().Version.Build, "O programie", MessageBoxButton.OK, MessageBoxImage.Information);
        private void ChangeLanguage_Click(object sender, RoutedEventArgs e) => WindowsManager.OpenWindow(Constants.CONST_CHANGE_LANGUAGE_WINDOW);

        private void SaveImage_Click(object sender, RoutedEventArgs e) => SaveImageToFile();

        private void DuplicateImage_Click(object sender, RoutedEventArgs e) => DuplicateImage();
     
        private void TcImagePages_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            MIAddWindows.IsEnabled = IsGrayImageFromSelectedTab();
            MIImageProcessings.IsEnabled = IsGrayImageFromSelectedTab();
        }

        #endregion

    }
}
