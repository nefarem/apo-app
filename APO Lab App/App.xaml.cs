﻿using APO_Lab_App.Classes.Language;
using System;
using System.Globalization;
using System.Threading;
using System.Windows;

namespace APO_Lab_App
{
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            string languageCode = APO_Lab_App.Properties.Settings.Default.CurrentLanguageCode;

            if (String.IsNullOrEmpty(languageCode))
            {
                CultureInfo culture = CultureInfo.CurrentUICulture;
                languageCode = LanguageFabric.languages.ContainsKey(culture.Name) ? culture.Name : "en-US";
            }

            Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageCode);

            base.OnStartup(e);
        }


    }
}
