﻿using APO_Lab_App.Classes.Events;
using APO_Lab_App.Classes.Interfaces;
using LiveChartsCore;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore.SkiaSharpView.Painting;
using SkiaSharp;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace APO_Lab_App.Windows
{
    public partial class PlotProfile : Window, IWindow
    {
        public List<int> GrayValues { get; private set; }
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey = string.Empty;

        public PlotProfile(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();
            SubscribeEvent();
            SetWindowTitle();
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void DrawPlot(List<int> values)
        {
            LineSeries<int> columnSeries = new LineSeries<int>
            {
                Values = values,
                Stroke = new SolidColorPaint(SKColors.Black),
                GeometryFill = null,
                GeometryStroke = null,
                GeometrySize = 0
            };

            ChartProfilePlot.Series = new ISeries[]
            {
                columnSeries
            };
        }

        public void SubscribeEvent()
        {
            SelectedPointsOnImage.SelectedPointsOnImageEvent += OnSelectedPointsOnImageEvent;
        }

        private void UnsubscribeEvent()
        {
            SelectedPointsOnImage.SelectedPointsOnImageEvent -= OnSelectedPointsOnImageEvent;
        }

        private void OnSelectedPointsOnImageEvent(object sender, int pageID, List<System.Drawing.Point> points, List<int> pixels)
        {
            DrawPlot(pixels);
        }

        private void TitlePlotProfile_Closing(object sender, CancelEventArgs e)
        {
            HideWindow(e);
        }
    }
}
