﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.ImageProcessing;
using APO_Lab_App.Classes.Interfaces;
using System.ComponentModel;
using System.Drawing;
using System.Windows;

namespace APO_Lab_App.Windows
{
    /// <summary>
    /// Interaction logic for ThresholdGrayLevelWindow.xaml
    /// </summary>
    public partial class ThresholdGrayLevelWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private bool canDrawImage;

        private int p1;
        private int p2;

        private Bitmap sourceBitmap;
        private Bitmap current;

        public ThresholdGrayLevelWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();

            BtnApply.IsEnabled = false;
            BtnReset.IsEnabled = false;

            //sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;
            //ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(current, "_thresholdGrayLevel");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            current = ImageProcessing.ThresholdingWithGrayLevel(sourceBitmap, p1, p2);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(current);
        }

        public void Reset()
        {
            NUDp1.Value = 0;
            NUDp2.Value = 0;
            ImagePreview.Source = null;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void ToggleButtons(bool value)
        {
            BtnApply.IsEnabled = value;
            BtnReset.IsEnabled = value;
        }

        private void Thresholdp1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            canDrawImage = NUDp1.Value >= 0 && NUDp2.Value >= 0;

            ToggleButtons(canDrawImage);

            if (canDrawImage)
            {
                p1 = (int)NUDp1.Value;
                p2 = (int)NUDp2.Value;

                DrawPreview();
            }
  
        }

        private void Thresholdp2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            canDrawImage = NUDp1.Value >= 0 && NUDp2.Value >= 0;

            ToggleButtons(canDrawImage);

            if (canDrawImage)
            {
                p1 = (int)NUDp1.Value;
                p2 = (int)NUDp2.Value;

                DrawPreview();
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
            }
        }
    }
}
