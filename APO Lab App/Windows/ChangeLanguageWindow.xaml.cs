﻿using APO_Lab_App.Classes.Interfaces;
using APO_Lab_App.Classes.Language;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace APO_Lab_App.Windows
{
    public partial class ChangeLanguageWindow : Window, IWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }
        private string windowNameKey;

        public ChangeLanguageWindow(string windowNameKey)
        {
            this.windowNameKey = windowNameKey;

            InitializeComponent();

            LoadLanguages();
        }

        public void SetWindowTitle() => Title = windowNameKey;

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void LoadLanguages()
        {
            CBLanguages.Items.Clear();

            foreach (string langKey in LanguageFabric.GetLanguageKeys())
                CBLanguages.Items.Add(LanguageFabric.languages[langKey]);
        }

        private void CBLanguages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string languageCode = LanguageFabric.GetLanguageKey(CBLanguages.SelectedItem.ToString());
            MainWindow.Instance.CurrentLanguageCode = languageCode;
            MainWindow.Instance.ChangeLanguage(languageCode);
        }
    }
}
