﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.ImageProcessing;
using APO_Lab_App.Classes.Interfaces;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;

namespace APO_Lab_App.Windows
{
    public partial class MedianFilterWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private int medianFilterValue;

        private Bitmap sourceBitmap;
        private Bitmap current;

        public MedianFilterWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();

            BtnApply.IsEnabled = false;
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(current, "_medianFilter");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            current = ImageProcessing.MedianFilter(sourceBitmap, medianFilterValue);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(current);
        }

        public void Reset()
        {
            ImagePreview.Source = null;
            BtnApply.IsEnabled = false;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void CBMedianFilters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!BtnApply.IsEnabled) BtnApply.IsEnabled = true;

            switch (CBMedianFilters.SelectedItem)
            {
                case "3x3":
                    medianFilterValue = 3;
                    break;

                case "5x5":
                    medianFilterValue = 5;
                    break;

                case "7x7":
                    medianFilterValue = 7;
                    break;
            }

            DrawPreview();
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);

                CBMedianFilters.Items.Clear();
                CBMedianFilters.Items.Add("3x3");
                CBMedianFilters.Items.Add("5x5");
                CBMedianFilters.Items.Add("7x7");

                CBMedianFilters.SelectedIndex = 0;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e) => HideWindow(e);

    }
}
