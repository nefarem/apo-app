﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.ImageProcessing;
using APO_Lab_App.Classes.Interfaces;
using System.ComponentModel;
using System.Drawing;
using System.Windows;

namespace APO_Lab_App.Windows
{
    public partial class ThresholdWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private int thresholdValue;

        private Bitmap sourceBitmap;
        private Bitmap current;

        public ThresholdWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();

            BtnApply.IsEnabled = false;
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(current, "_threshold");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            current = ImageProcessing.Thresholding(sourceBitmap, thresholdValue);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(current);
        }

        public void Reset()
        {
            SlThreshold.Value = 0;
            ImagePreview.Source = null;
            LbThresholdValue.Content = 0;
            BtnApply.IsEnabled = false;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        { 
            if(!BtnApply.IsEnabled) BtnApply.IsEnabled = true;

            thresholdValue = (int)SlThreshold.Value;
            LbThresholdValue.Content = (int)SlThreshold.Value;
            DrawPreview();
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e) => HideWindow(e);
    }
}
