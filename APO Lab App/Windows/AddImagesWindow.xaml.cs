﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.Interfaces;
using Emgu.CV;
using Emgu.CV.Structure;
using System.ComponentModel;
using System.Drawing;
using System.Windows;

namespace APO_Lab_App.Windows
{
    public partial class AddImagesWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private Bitmap sourceBitmap;
        private Image<Gray, byte> currentImage;
        private Image<Gray, byte> resultImage;
        private Image<Gray, byte> loadedImageGray;

        public AddImagesWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(resultImage.ToBitmap(), "_universalLine");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            resultImage = new Image<Gray, byte>(currentImage.Width, currentImage.Height);
            Image<Gray, byte> currentImageGray = currentImage?.Convert<Gray, byte>();

            CvInvoke.Add(currentImageGray, loadedImageGray, resultImage);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(resultImage.ToBitmap());

            BtnApply.IsEnabled = true;
        }

        public void Reset()
        {
            ImagePreview.Source = null;
            ImageLoaded.Source = null;
            BtnApply.IsEnabled = false;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void BtnLoadImage_Click(object sender, RoutedEventArgs e)
        {
            Image<Bgr, byte> loadedImage = MainWindow.Instance.LoadImageFromFile();
            if (loadedImage == null) return;

            loadedImageGray = loadedImage?.Convert<Gray, byte>();
            ImageLoaded.Source = BitmapConverter.BitmapToBitmapImageConvert(loadedImageGray.ToBitmap());

            DrawPreview();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.GetGrayImageFromSelectedTab().ToBitmap();
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
                currentImage = MainWindow.Instance.GetGrayImageFromSelectedTab();

                BtnApply.IsEnabled = false;
            }
        }
        private void Window_Closing(object sender, CancelEventArgs e) => HideWindow(e);
    }
}
