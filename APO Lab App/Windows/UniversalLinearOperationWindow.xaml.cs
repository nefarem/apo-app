﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.Interfaces;
using Emgu.CV;
using Emgu.CV.Structure;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Drawing;
using System.Windows;

namespace APO_Lab_App.Windows
{
    public partial class UniversalLinearOperationWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private Bitmap sourceBitmap;
        private Image<Gray, byte> currentImage;
        private Image<Gray, byte> resultImage;

        private NumericUpDown[,] numericUpDownControls = new NumericUpDown[3, 3];
        private float[,] numericUpDownValues = new float[3, 3];

        public UniversalLinearOperationWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(resultImage.ToBitmap(), "universal_line");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            float[,] tmpKernel = new float[3, 3];

            for (int x = 0; x < tmpKernel.GetLength(0); ++x)
                for (int y = 0; y < tmpKernel.GetLength(1); ++y)
                    tmpKernel[x, y] = (int)numericUpDownControls[x, y].Value;

            Matrix<float> matrixKernel = new Matrix<float>(tmpKernel);
            Matrix<float> interactiveKernel = new Matrix<float>(numericUpDownValues);
            Matrix<float> mask = matrixKernel / interactiveKernel.Sum;

            resultImage = new Image<Gray, byte>(sourceBitmap.Width, sourceBitmap.Height);

            CvInvoke.Filter2D(currentImage, resultImage, mask, new System.Drawing.Point(-1, -1), 0, Emgu.CV.CvEnum.BorderType.Replicate);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(resultImage.ToBitmap());
        }

        public void Reset()
        {
            ImagePreview.Source = null;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void GetValuesFromCells()
        {
            for (int i = 0; i < numericUpDownControls.GetLength(0); ++i)
                for (int j = 0; j < numericUpDownControls.GetLength(1); ++j)
                    numericUpDownValues[i, j] = (int)numericUpDownControls[i, j].Value;
        }

        private void NUDCell_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
           if (!IsVisible) return;

           GetValuesFromCells();
           DrawPreview();
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }
        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                // :(
                numericUpDownControls[0, 0] = Grid00;
                numericUpDownControls[0, 1] = Grid01;
                numericUpDownControls[0, 2] = Grid02;
                numericUpDownControls[1, 0] = Grid10;
                numericUpDownControls[1, 1] = Grid11;
                numericUpDownControls[1, 2] = Grid12;
                numericUpDownControls[2, 0] = Grid20;
                numericUpDownControls[2, 1] = Grid21;
                numericUpDownControls[2, 2] = Grid22;

                sourceBitmap = MainWindow.Instance.GetGrayImageFromSelectedTab().ToBitmap();
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
                currentImage = MainWindow.Instance.GetGrayImageFromSelectedTab();
            }
        }
        private void Window_Closing(object sender, CancelEventArgs e) => HideWindow(e);
    }
}
