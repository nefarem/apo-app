﻿using APO_Lab_App.Classes.Interfaces;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;

namespace APO_Lab_App.Windows
{
    public partial class FilterTwoStageWindow : IProcessingImageWindow
    {
        private List<NumericUpDown> mask1Controls;
        private List<NumericUpDown> mask2Controls;
        private List<NumericUpDown> mask3Controls;

        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private Bitmap sourceBitmap;
        private Bitmap current;

        public FilterTwoStageWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            mask1Controls = new List<NumericUpDown>();
            mask2Controls = new List<NumericUpDown>();
            mask3Controls = new List<NumericUpDown>();

            InitializeComponent();

            BtnApply.IsEnabled = false;
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(current, "_filterTwoStage");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            //current = ImageProcessing.Thresholding(sourceBitmap, thresholdValue);
            //ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(current);

            //float[,] kernel1 = new float[3, 3];
            //float[,] kernel2 = new float[3, 3];

            //int tmp = 0;

            //for (int x = 0; x < kernel1.GetLength(0); ++x)
            //    for (int y = 0; y < kernel1.GetLength(1); ++y)
            //    {
            //        kernel1[x, y] = (int)mask1Controls[tmp].Value;
            //        tmp++;
            //    }

            //tmp = 0;
            //for (int x = 0; x < kernel2.GetLength(0); ++x)
            //    for (int y = 0; y < kernel2.GetLength(1); ++y)
            //    {
            //        kernel2[x, y] = (int)mask2Controls[tmp].Value;
            //        tmp++;
            //    }

            //ConvolutionKernelF convolutionKernel1 = new ConvolutionKernelF(kernel1);
            //ConvolutionKernelF convolutionKernel2 = new ConvolutionKernelF(kernel2);
            //ConvolutionKernelF convolutionKernel3 = new ConvolutionKernelF(5, 5);

            //Matrix<float> kernel3 = new Matrix<float>(kernel1);
            //Matrix<float> kernel4 = new Matrix<float>(kernel2);
            //var kernel5 = kernel3 * kernel4;
        }

        public void Reset()
        {
            ResetValuesInControls();
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        // Todo: Refactor this! Zrobione na szybko przed prezentacją na laby
        private void SetControlsToList()
        {
            mask1Controls.Clear();
            mask2Controls.Clear();
            mask3Controls.Clear();

            mask1Controls.Add(NUDMask11);
            mask1Controls.Add(NUDMask12);
            mask1Controls.Add(NUDMask13);
            mask1Controls.Add(NUDMask14);
            mask1Controls.Add(NUDMask15);
            mask1Controls.Add(NUDMask16);
            mask1Controls.Add(NUDMask17);
            mask1Controls.Add(NUDMask18);
            mask1Controls.Add(NUDMask19);

            mask2Controls.Add(NUDMask21);
            mask2Controls.Add(NUDMask22);
            mask2Controls.Add(NUDMask23);
            mask2Controls.Add(NUDMask24);
            mask2Controls.Add(NUDMask25);
            mask2Controls.Add(NUDMask26);
            mask2Controls.Add(NUDMask27);
            mask2Controls.Add(NUDMask28);
            mask2Controls.Add(NUDMask29);

            mask3Controls.Add(NUDMask31);
            mask3Controls.Add(NUDMask32);
            mask3Controls.Add(NUDMask33);
            mask3Controls.Add(NUDMask34);
            mask3Controls.Add(NUDMask35);
            mask3Controls.Add(NUDMask36);
            mask3Controls.Add(NUDMask37);
            mask3Controls.Add(NUDMask38);
            mask3Controls.Add(NUDMask39);
            mask3Controls.Add(NUDMask310);
            mask3Controls.Add(NUDMask311);
            mask3Controls.Add(NUDMask312);
            mask3Controls.Add(NUDMask313);
            mask3Controls.Add(NUDMask314);
            mask3Controls.Add(NUDMask315);
            mask3Controls.Add(NUDMask316);
            mask3Controls.Add(NUDMask317);
            mask3Controls.Add(NUDMask318);
            mask3Controls.Add(NUDMask319);
            mask3Controls.Add(NUDMask320);
            mask3Controls.Add(NUDMask321);
            mask3Controls.Add(NUDMask322);
            mask3Controls.Add(NUDMask323);
            mask3Controls.Add(NUDMask324);
            mask3Controls.Add(NUDMask325);
        }

        private void ResetValuesInControls()
        {
            foreach (NumericUpDown numericUpDown in mask1Controls) numericUpDown.Value = 0;
            foreach (NumericUpDown numericUpDown in mask2Controls) numericUpDown.Value = 0;
            foreach (NumericUpDown numericUpDown in mask3Controls) numericUpDown.Value = 0;
        }
     
        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void AnyMask_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            if (mask1Controls.Count == 0 | mask2Controls.Count == 0 | mask3Controls.Count == 0) return;
            DrawPreview();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;

                SetControlsToList();
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e) => HideWindow(e);

    }
}
