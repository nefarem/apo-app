﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.ImageProcessing;
using APO_Lab_App.Classes.Interfaces;
using System.ComponentModel;
using System.Drawing;
using System.Windows;

namespace APO_Lab_App.Windows
{
    /// <summary>
    /// Interaction logic for ThresholdGrayLevelWindow.xaml
    /// </summary>
    public partial class StretchRangeWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private bool canDrawImage;

        private int p1;
        private int p2;
        private int q3;
        private int q4;

        private Bitmap sourceBitmap;
        private Bitmap current;

        public StretchRangeWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();

            BtnApply.IsEnabled = false;
            BtnReset.IsEnabled = false;
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(current, "_thresholdGrayLevel");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            current = ImageProcessing.StretchingRange(sourceBitmap, p1, p2, q3, q4);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(current);
        }

        public void Reset()
        {
            NUDp1.Value = 0;
            NUDp2.Value = 0;
            NUDq3.Value = 0;
            NUDq4.Value = 0;

            ImagePreview.Source = null;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void ToggleButtons(bool value)
        {
            BtnApply.IsEnabled = value;
            BtnReset.IsEnabled = value;
        }

        private void CheckConditionsAndDraw()
        {
            canDrawImage = NUDp1.Value >= 0 && NUDp2.Value >= 0 && NUDq3.Value >= 0 && NUDq4.Value >= 0;

            ToggleButtons(canDrawImage);

            if (canDrawImage)
            {
                p1 = (int)NUDp1.Value;
                p2 = (int)NUDp2.Value;
                q3 = (int)NUDq3.Value;
                q4 = (int)NUDq4.Value;

                DrawPreview();
            }
        }

        private void StretchRangep1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e) => CheckConditionsAndDraw();
        private void StretchRangep2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e) => CheckConditionsAndDraw();
        private void StretchRangeq3_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e) => CheckConditionsAndDraw();
        private void StretchRangeq4_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e) => CheckConditionsAndDraw();

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void Window_Closing(object sender, CancelEventArgs e) => HideWindow(e);

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
            }
        }
    }
}
