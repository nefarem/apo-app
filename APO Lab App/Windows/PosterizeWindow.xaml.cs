﻿using APO_Lab_App.Classes.Converter;
using APO_Lab_App.Classes.ImageProcessing;
using APO_Lab_App.Classes.Interfaces;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;

namespace APO_Lab_App.Windows
{
    public partial class PosterizeWindow : IProcessingImageWindow
    {
        public string WindowNameKey
        {
            get => windowNameKey;
            set => windowNameKey = value;
        }

        private string windowNameKey;

        private int posterizeBinsNum;

        private Bitmap sourceBitmap;
        private Bitmap current;

        public PosterizeWindow(string windowNameKey)
        {
            this.WindowNameKey = windowNameKey;

            InitializeComponent();

            BtnApply.IsEnabled = false;
            BtnReset.IsEnabled = false;
        }

        public void Apply()
        {
            MainWindow.Instance.PrepareViewTabAfterImageProcessing(current, "_posterize");
        }

        public void HideWindow(CancelEventArgs e)
        {
            MainWindow.Instance.WindowsManager.CloseWindow(windowNameKey);

            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        public void DrawPreview()
        {
            current = ImageProcessing.Posterize(sourceBitmap, posterizeBinsNum);
            ImagePreview.Source = BitmapConverter.BitmapToBitmapImageConvert(current);
        }

        public void Reset()
        {
            TbPosterizeValue.Text = string.Empty;
            ImagePreview.Source = null;
        }

        public void SetWindowTitle()
        {
            Title = windowNameKey;
        }

        private void PosterizeValue_Changed(object sender, TextChangedEventArgs e)
        {
            bool result = int.TryParse(TbPosterizeValue.Text, out int value);
            TbPosterizeValue.Background = result ? System.Windows.Media.Brushes.White : System.Windows.Media.Brushes.Red;

            BtnApply.IsEnabled = result;
            BtnReset.IsEnabled = result;

            if (result)
            {
                posterizeBinsNum = value;
                DrawPreview();
            }
                
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Apply();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                sourceBitmap = MainWindow.Instance.TabPageViewModel.TabPageDatas[MainWindow.Instance.TcImagePages.SelectedIndex].TabImageData.Bitmap;
                ImageSource.Source = BitmapConverter.BitmapToBitmapImageConvert(sourceBitmap);
            }
        }
    }
}
