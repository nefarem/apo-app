﻿using System.Collections.Generic;
using System.Drawing;

namespace APO_Lab_App.Classes.Events
{
    public delegate void SelectedPointsOnImageEventHandler(object sender, int pageID, List<Point> points, List<int> pixels);

    /// <summary>
    /// Static class with event SelectedPointsOnImage which is invoke after click second point on image durring draw profile line. Event is subscribe in constructor and unsubsribe in closing event.
    /// </summary>
    public class SelectedPointsOnImage
    {
        /// <summary>
        /// Event data
        /// </summary>
        public static event SelectedPointsOnImageEventHandler SelectedPointsOnImageEvent;

        /// <summary>
        /// Invoke event.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="pageID">PageID</param>
        /// <param name="points">Points</param>
        /// <param name="pixels">Pixels</param>
        public static void SelectedPointsOnImageInvoke(object sender, int pageID, List<Point> points, List<int> pixels) => SelectedPointsOnImageEvent?.Invoke(sender, pageID, points, pixels);  
    }
}
