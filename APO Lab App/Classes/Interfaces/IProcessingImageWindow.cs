﻿using System.ComponentModel;

namespace APO_Lab_App.Classes.Interfaces
{
    /// <summary>
    /// Interface with methods and properties for additional windows.
    /// </summary>
    public interface IWindow
    {
        /// <summary>
        /// Unique window name.
        /// </summary>
        string WindowNameKey { get; set; }

        /// <summary>
        /// Set window title.
        /// </summary>
        void SetWindowTitle();

        /// <summary>
        /// Hide window, this method should overide Close method from Window class.
        /// </summary>
        /// <param name="e"></param>
        void HideWindow(CancelEventArgs e);
    }

    /// <summary>
    /// Interface with methods and properties for additional windows with image processing. Inheritance from interface IWindow.
    /// </summary>
    public interface IProcessingImageWindow : IWindow
    {
        /// <summary>
        /// Draw preview for image with specific params.
        /// </summary>
        void DrawPreview();

        /// <summary>
        /// Apply preview and set to image list in main window.
        /// </summary>
        void Apply();

        /// <summary>
        /// Reset all user preview settings to default values.
        /// </summary>
        void Reset();
    }
}
