﻿using APO_Lab_App.Classes.Histogram;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace APO_Lab_App.Classes.ImageProcessing
{
    /// <summary>
    /// Static class with all methods to image processing.
    /// </summary>
    public static class ImageProcessing
    {

        #region Image processing methods with own code
        /// <summary>
        /// Stretch histogram.
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="histogramGrayscaleData">Gray image histogram which will be streched.</param>
        /// <returns>Bitmap image with streched histogram.</returns
        public static Bitmap StretchHistogram(Bitmap image)
        {
            /*
               Logic:
               1. Iterate image and find min and max pixelValue.
               2. Calculate stretching = ( (pixelValue - min) * newMax) / (max - min) ).
               3. Change pixels from resultTab.
            */

            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);

            int[,] resultTab = new int[image.Width, image.Height];

            bool findedStart = false;

            int min = 0;
            int max = 0;

            int newMax = 255;

            // Find min, max
            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    int pixelValue = image.GetPixel(x, y).R;

                    if (pixelValue == 0 && findedStart) findedStart = false;

                    if (pixelValue > 0 && !findedStart)
                    {
                        findedStart = true;
                        min = pixelValue;
                        max = pixelValue;
                    }
                    else if (findedStart && pixelValue < min) min = pixelValue;
                    else if (findedStart && pixelValue > max) max = pixelValue;
                }
            }

            // Calculate stretching
            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    int pixelValue = image.GetPixel(x, y).R;
                    resultTab[x, y] = ((pixelValue - min) * newMax) / (max - min);

                    Color color = Color.FromArgb(resultTab[x, y], resultTab[x, y], resultTab[x, y]);

                    result.SetPixel(x, y, color);
                }
            }

            return result;
        }


        /// <summary>
        /// Equalises a narrowed histogram for image. Only for gray images.
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="histogramGrayscaleData">Gray image histogram which will be equalize.</param>
        /// <returns>Bitmap image with equalized histogram.</returns
        public static Bitmap EqualizeHistogram(Bitmap image, HistogramGrayscaleData histogramGrayscaleData)
        {
            /*
              Logic:
              1. Calculate probability = 255 / (image.Width * image.Height)
              2. Calculate cumulative sum indexed from 1
              3. Normalize histogram
              4. Change pixels from normalized histogram
             */

            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);

            // Calculate probability
            double probability = (double)255 / (image.Width * image.Height);

            // Calculate cumulative sum
            double[] cumulativeSumHistogram = new double[256];

            for (int i = 1; i < cumulativeSumHistogram.Length; ++i)
                cumulativeSumHistogram[i] = (double)cumulativeSumHistogram[i - 1] + histogramGrayscaleData.Frequency[i];

            // Normnalize histogram
            int[] normalizedHistogram = new int[256];

            for (int i = 0; i < normalizedHistogram.Length; ++i)
            {
                cumulativeSumHistogram[i] = cumulativeSumHistogram[i] * probability;
                normalizedHistogram[i] = (int)cumulativeSumHistogram[i];
            }

            // Change pixels
            int grayPixelValue;
            Color color;

            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    grayPixelValue = image.GetPixel(x, y).R;
                    color = Color.FromArgb(normalizedHistogram[grayPixelValue], normalizedHistogram[grayPixelValue], normalizedHistogram[grayPixelValue]);
                    result.SetPixel(x, y, color);
                }
            }
            return result;
        }

        /// <summary>
        /// The negation or negation of an image, in the simplest way, can be calculated by subtracting the image values from the maximum brightness value of the range.
        /// </summary>
        /// <param name="image">Gray image to negation</param>
        /// <returns>Image after negation operation</returns>
        public static Bitmap Negation(Bitmap image)
        {
            /*
               Logic:
               1. Iterate on all pixels in image and subtraction from 255, result put as pixel value in result bitmap.
            */

            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);

            for (int x = 0; x < image.Width; ++x)
                for (int y = 0; y < image.Height; ++y)
                {
                    Color color = Color.FromArgb(255 - image.GetPixel(x, y).R, 255 - image.GetPixel(x, y).R, 255 - image.GetPixel(x, y).R);
                    result.SetPixel(x, y, color);
                }

            return result;
        }


        /// <summary>
        /// Make threshold operation on gray image with threshold value taken from Constans class.
        /// </summary>
        /// <param name="image">Gray image to threshold</param>
        /// <param name="thresholdingValue">Threshold value</param>
        /// <returns>Image after threshold operation</returns>
        public static Bitmap Thresholding(Bitmap image, int thresholdingValue)
        {

            /*
                Logic:
                1. Iterate on all image and if pixelValue[x, y] > threshold_value then to thresholdTab[x, y] put value -> 1.
                2. Multiplay all elements in thresholdTab * 255.
                3. Set pixels from thresholdTab.
           */

            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);

            int[,] thresholdTab = new int[result.Width, result.Height];
            int pixelValue;

            // Check condition pixelValue[x, y] > threshold_value
            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < result.Height; ++y)
                {
                    pixelValue = result.GetPixel(x, y).R;

                    if (pixelValue >= thresholdingValue) thresholdTab[x, y] = 1;
                }

            // Multiplay all elements in thresholdTab * 255
            for (int x = 0; x < result.Width; ++x)
                for (int y = 0; y < result.Height; ++y)
                    if (thresholdTab[x, y] > 0) thresholdTab[x, y] = thresholdTab[x, y] * 255;

            // Set pixels from thresholdTab and return
            return SetPixels(result, thresholdTab);
        }

        /// <summary>
        /// Make threshold operation on gray image with threshold gray level.
        /// </summary>
        /// <param name="image">Gray image to thresholding.</param>
        /// <param name="p1">Min pixel value</param>
        /// <param name="p2">Max pixel value</param>
        /// <returns></returns>
        public static Bitmap ThresholdingWithGrayLevel(Bitmap image, int p1, int p2)
        {

            /*
              Logic:
              1. Iterate on all image and if pixelValue[x, y] is between p1 and p2 then to thresholdTab[x, y] put value -> pixelValue.
              2. Set pixels from thresholdTab.
            */

            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);

            int[,] thresholdTab = new int[result.Width, result.Height];
            int pixelValue = 0;

            for (int x = 0; x < result.Width; ++x)
            {
                for (int y = 0; y < result.Height; ++y)
                {
                    pixelValue = result.GetPixel(x, y).R;

                    if (pixelValue >= p1 && pixelValue <= p2)
                        thresholdTab[x, y] = pixelValue;
                }
            }

            // Set pixels from thresholdTab and return
            return SetPixels(result, thresholdTab);
        }


        /// <summary>
        /// Reduction of grey levels by posterisation.
        /// </summary>
        /// <param name="image">Gray image to posterize</param>
        /// <param name="posterizeBinsNum">Posterize bins numbers</param>
        /// <returns>Image after posterize operation</returns>
        public static Bitmap Posterize(Bitmap image, int posterizeBinsNum)
        {
            /*
                 Logic:
                 1. Calculate binLenght = (int) 255 / Constatns.CONST_POSTERIZE_BINS_NUM (8).
                 2. Add to array with size Constatns.CONST_POSTERIZE_BINS_NUM (8), next values added by binLenght.
                 3. Iterate on all pixels in image, and additional itera on array with bins, check condition: currentPixelValue > binsArray[i], if true put value to array -> resultTab[x, y] = binsArray[i]
                 4. Check condition currentPixelValue > posterizeBins.LastOrDefault()), if true put value to array -> resultTab[x, y] = 255.
                 5. Set pixels to result image from resultTab.
            */

            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);
            int[,] pixelsTab = new int[image.Width, image.Height];

            double numberToRound = 255d / posterizeBinsNum;

            int binLenght = (int)Math.Round(numberToRound, MidpointRounding.AwayFromZero);

            int[] binsNumTab = new int[posterizeBinsNum];
            binsNumTab[0] = 0;

            for (int i = 1; i < posterizeBinsNum; ++i)
                binsNumTab[i] = binsNumTab[i - 1] + binLenght;

            int pixelValue = 0;

            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    pixelValue = image.GetPixel(x, y).R;

                    for (int i = 0; i < binsNumTab.Length; ++i)
                        if (pixelValue > binsNumTab[i]) pixelsTab[x, y] = binsNumTab[i];

                    if (pixelValue > binsNumTab[binsNumTab.Length - 1]) pixelsTab[x, y] = 255;
                }
            }

            for (int x = 0; x < image.Width; ++x)
                for (int y = 0; y < image.Height; ++y)
                {
                    Color color = Color.FromArgb(pixelsTab[x, y], pixelsTab[x, y], pixelsTab[x, y]);
                    result.SetPixel(x, y, color);
                }

            return result;
        }


        /// <summary>
        /// Stretching image for specific range.
        /// </summary>
        /// <param name="image">Image to stretching range</param>
        /// <param name="p1">Min pixel value</param>
        /// <param name="p2">Max pixel value</param>
        /// <param name="q3">Min range value</param>
        /// <param name="q4">Max range value</param>
        /// <returns>Image after streaching</returns>
        public static Bitmap StretchingRange(Bitmap image, int p1, int p2, int q3 = 0, int q4 = 255)
        {
            /*
               Logic:
               1. Iterate on all image and if pixelValue[x, y] is betwwen p1 and p2 then to pixelsTab[x, y] put value = (pixelValue - p1) * (((q4 - q3) / (p2 - p1)) + q3)
               2. Set pixels to result image from pixelsTab.
            */


            Bitmap result = image.Clone(new Rectangle(0, 0, image.Width, image.Height), PixelFormat.Format24bppRgb);
            int[,] pixelsTab = new int[image.Width, image.Height];

            int pixelValue = 0;

            for (int x = 0; x < result.Width; ++x)
            {
                for (int y = 0; y < result.Height; ++y)
                {
                    pixelValue = result.GetPixel(x, y).R;

                    if (p1 <= pixelValue && pixelValue <= p2)
                    {
                        int calcResult = (pixelValue - p1) * (((q4 - q3) / (p2 - p1)) + q3);
                        pixelsTab[x, y] = calcResult <= 255 ? calcResult : 255;
                    }
                        
                }
            }

            for (int x = 0; x < image.Width; ++x)
                for (int y = 0; y < image.Height; ++y)
                {
                    Color color = Color.FromArgb(pixelsTab[x, y], pixelsTab[x, y], pixelsTab[x, y]);
                    result.SetPixel(x, y, color);
                }

            return result;
        }

        #endregion

        #region Image processing methods invoke from EmguCV framework

        /// <summary>
        /// Make linear smoothing blur using information from kernel.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after linear smoothing blur.</returns>
        public static Bitmap LinearSmoothingBlur(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            CvInvoke.Blur(image.ToImage<Gray, byte>(), resultImage, Constants.CONST_KERNEL_SIZE, new System.Drawing.Point(-1, -1));
            
            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make linear smoothing Gauss blur using information from kernel.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap LinearSmoothingGaussBlur(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            double sigmaX = 0;

            CvInvoke.GaussianBlur(image.ToImage<Gray, byte>(), resultImage, Constants.CONST_KERNEL_SIZE, sigmaX, 0, Emgu.CV.CvEnum.BorderType.Replicate);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Edge detection using Sobel operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap EdgeDetectionSobel(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();

            CvInvoke.Sobel(image.ToImage<Gray, byte>(), resultImage, Emgu.CV.CvEnum.DepthType.Cv8U, 1, 0, 5);
            
            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Edge detection using Laplacian operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap EdgeDetectionLaplacian(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();

            CvInvoke.Sobel(image.ToImage<Gray, byte>(), resultImage, Emgu.CV.CvEnum.DepthType.Cv8U, 1, 0, 5);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Edge detection using Canny operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap EdgeDetectionCanny(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            Image<Gray, byte> resultat = resultImage.Canny(50, 20);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Edge detection using Prewitt with 8 directions operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap EdgeDetectionPrewitt(Bitmap image)
        {
            float[,] kernel = new float[,]
            {
                {-1, -1, 0 },
                {-1, 0, 1 },
                {0, 1, 1 }
            };

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel);

            CvInvoke.Filter2D(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 0, Emgu.CV.CvEnum.BorderType.Replicate);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make linear sharp for specific kernels.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap LinearSharp(Bitmap image)
        {
            float[,] kernel1 = new float[,]
            {
                {0, -1, 0},
                {-1, 5, -1 },
                {0, -1, 0 }
            };

            float[,] kernel2 = new float[,]
            {
                {-1, -1, -1},
                {-1, 9, -1 },
                {-1, -1, -1 }
            };

            float[,] kernel3 = new float[,]
            {
                {1, -2, 1},
                {-2, 5, -2 },
                {1, -2, 1 }
            };


            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel2);

            CvInvoke.Filter2D(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 0, Emgu.CV.CvEnum.BorderType.Replicate);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make median filter for specific value, 3x3, 5x5, 7x7.
        /// </summary>
        /// <param name="image">Image to </param>
        /// <param name="medianFilterValue">Median filter value</param>
        /// <returns></returns>
        public static Bitmap MedianFilter(Bitmap image, int medianFilterValue)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();

            CvInvoke.MedianBlur(image.ToImage<Gray, byte>(), resultImage, medianFilterValue);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make morphology erode operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap Erode(Bitmap image)
        {
            float[,] kernel = new float[3, 3];

            for (int i = 0; i < kernel.GetLength(0); ++i)
                for (int j = 0; j < kernel.GetLength(1); ++j)
                    kernel[i, j] = 1;


            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel);

            CvInvoke.Erode(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 3, Emgu.CV.CvEnum.BorderType.Replicate, new MCvScalar(0));

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make morphology dilate operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap Dilate(Bitmap image)
        {
            float[,] kernel = new float[3, 3];

            for (int i = 0; i < kernel.GetLength(0); ++i)
                for (int j = 0; j < kernel.GetLength(1); ++j)
                    kernel[i, j] = 1;

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel);

            CvInvoke.Dilate(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 3, Emgu.CV.CvEnum.BorderType.Replicate, new MCvScalar(0));

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make morphology open operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap OpenMorph(Bitmap image)
        {
            float[,] kernel = new float[3, 3];

            for (int i = 0; i < kernel.GetLength(0); ++i)
                for (int j = 0; j < kernel.GetLength(1); ++j)
                    kernel[i, j] = 1;

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel);

            CvInvoke.MorphologyEx(image.ToImage<Gray, byte>(), resultImage, Emgu.CV.CvEnum.MorphOp.Open, convolutionKernelF, new System.Drawing.Point(-1, -1), 3, Emgu.CV.CvEnum.BorderType.Replicate, new MCvScalar(0));

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make morphology close operation.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap CloseMorph(Bitmap image)
        {
            float[,] kernel = new float[3, 3];

            for (int i = 0; i < kernel.GetLength(0); ++i)
                for (int j = 0; j < kernel.GetLength(1); ++j)
                    kernel[i, j] = 1;

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel);

            CvInvoke.MorphologyEx(image.ToImage<Gray, byte>(), resultImage, Emgu.CV.CvEnum.MorphOp.Close, convolutionKernelF, new System.Drawing.Point(-1, -1), 3, Emgu.CV.CvEnum.BorderType.Replicate, new MCvScalar(0));

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make filter one stage with smooth effect.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap FiltrOneStageSmooth(Bitmap image)
        {
            float[,] kernel1 = new float[3, 3];

            for (int i = 0; i < kernel1.GetLength(0); ++i)
                for (int j = 0; j < kernel1.GetLength(1); ++j)
                    kernel1[i, j] = 1;

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel1);

            CvInvoke.Filter2D(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 0, Emgu.CV.CvEnum.BorderType.Replicate);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make filter one stage with sharp effect.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap FiltrOneStageSharp(Bitmap image)
        {
            float[,] kernel1 = new float[,]
            {
                {1, -2, 1 },
                {-2, 4, -2 },
                {1, -2, 1 },
            };

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel1);

            CvInvoke.Filter2D(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 0, Emgu.CV.CvEnum.BorderType.Replicate);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make filter two stage with smooth and sharp effect and resize kernel.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap FiltrTwoStages(Bitmap image)
        {
            float[,] kernel1 = new float[3, 3];

            for (int i = 0; i < kernel1.GetLength(0); ++i)
                for (int j = 0; j < kernel1.GetLength(1); ++j)
                    kernel1[i, j] = 1;

            float[,] kernel2 = new float[,]
            {
                {1, -2, 1 },
                {-2, 4, -2 },
                {1, -2, 1 },
            };

            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            ConvolutionKernelF convolutionKernelF = new ConvolutionKernelF(kernel1);
            convolutionKernelF.Add(new ConvolutionKernelF(kernel2));

            CvInvoke.Filter2D(image.ToImage<Gray, byte>(), resultImage, convolutionKernelF, new System.Drawing.Point(-1, -1), 0, Emgu.CV.CvEnum.BorderType.Replicate);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Image segmentation - method adaptive.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap Adaptive(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();

            CvInvoke.AdaptiveThreshold(image.ToImage<Gray, byte>(), resultImage, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.MeanC, Emgu.CV.CvEnum.ThresholdType.Binary, 11, 5);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Image segmentation - method Otsu.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap Otsu(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();

            CvInvoke.Threshold(image.ToImage<Gray, byte>(), resultImage, 0, 255, Emgu.CV.CvEnum.ThresholdType.Otsu);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Image segmentation - method Watershed.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap Watershed(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();

            CvInvoke.Threshold(image.ToImage<Gray, byte>(), resultImage, 0, 255, Emgu.CV.CvEnum.ThresholdType.Otsu);

            return resultImage.ToBitmap();
        }

        /// <summary>
        /// Make skeletisation for image.
        /// </summary>
        /// <param name="image">Image to processing</param>
        /// <returns>Image after image processing.</returns>
        public static Bitmap Skeletalisation(Bitmap image)
        {
            Image<Gray, byte> resultImage = image.ToImage<Gray, byte>();
            Image<Gray, byte> imageCopy = image.ToImage<Gray, byte>();

            Mat skelMat = new Mat(image.Width, image.Height, Emgu.CV.CvEnum.DepthType.Cv8U, 1);

            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Cross, new Size(3, 3), new Point(1, 1));

            Image<Gray, byte> im_open = new Image<Gray, byte>(image.Width, image.Height);
            Image<Gray, byte> im_temp = new Image<Gray, byte>(image.Width, image.Height);
            Image<Gray, byte> im_eroded = new Image<Gray, byte>(image.Width, image.Height);


            while (true)
            {
                CvInvoke.MorphologyEx(imageCopy, im_open, Emgu.CV.CvEnum.MorphOp.Open, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
                CvInvoke.Subtract(imageCopy, im_open, im_temp);
                CvInvoke.Erode(imageCopy, im_eroded, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar());
                CvInvoke.BitwiseOr(skelMat, im_temp, skelMat);
                imageCopy = im_eroded;

                if (CvInvoke.CountNonZero(imageCopy) == 0)
                    break;
     
            }

            resultImage = imageCopy;
            return resultImage.ToBitmap();

        }

        /// <summary>
        /// Get all pixels from image.
        /// </summary>
        /// <param name="image">Image</param>
        /// <returns> Int array with pixels from image.</returns>
        public static int[,] GetAllPixelsFromImage(Bitmap image)
        {
            int[,] result = new int[image.Width, image.Height];

            for (int x = 0; x < image.Width; ++x)
            {
                for (int y = 0; y < image.Height; ++y)
                {
                    result[x, y] = image.GetPixel(x, y).R;
                }
            }

            return result;
        }


        /// <summary>
        /// Set pixel from array in bitmap and return result.
        /// </summary>
        /// <param name="bitmap">Bitmap</param>
        /// <param name="pixelValues">Pixel values</param>
        /// <returns>Bitmap with set pixels from array</returns>
        private static Bitmap SetPixels(Bitmap bitmap, int[,] pixelValues)
        {
            Bitmap result = new Bitmap(bitmap.Width, bitmap.Height);

            for (int x = 0; x < bitmap.Width; ++x)
                for (int y = 0; y < bitmap.Height; ++y)
                {
                    Color color = Color.FromArgb(pixelValues[x, y], pixelValues[x, y], pixelValues[x, y]);
                    result.SetPixel(x, y, color);
                }

            return result;
        }


        #endregion





    }
}
