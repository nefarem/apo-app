﻿using System.Drawing;
using System.Windows.Media.Imaging;

namespace APO_Lab_App.Classes.TabImageDataSpace
{
    /// <summary>
    /// Base class with image data in tab.
    /// </summary>
    public abstract class TabImageData
    {
        /// <summary>
        /// Reference to image Bitmap object.
        /// </summary>
        public Bitmap Bitmap { get; protected set; }

        /// <summary>
        /// Reference to image BitmapImage object.
        /// </summary>
        public BitmapImage BitmapImage { get; protected set; }

        /// <summary>
        /// Local path to image.
        /// </summary>
        public string ImagePath { get; protected set; }
    }
}
