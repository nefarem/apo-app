﻿using APO_Lab_App.Classes.Converter;
using Emgu.CV;
using Emgu.CV.Structure;
using System;

namespace APO_Lab_App.Classes.TabImageDataSpace
{
    /// <summary>
    /// Class with RGB image data in tab. Inheritance from class TabImageData.
    /// </summary>
    public class TabImageDataRGB : TabImageData
    {
        /// <summary>
        /// Reference to RGBImage.
        /// </summary>
        public Image<Bgr, Byte> RGBImage { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="image">Image</param>
        public TabImageDataRGB(Image<Bgr, Byte> image)
        {
            this.RGBImage = image;
            this.Bitmap = image.ToBitmap();
            this.BitmapImage = BitmapConverter.BitmapToBitmapImageConvert(Bitmap);
        }

        /// <summary>
        /// Constructor for loaded from file image.
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="path">Path</param>
        public TabImageDataRGB(Image<Bgr, Byte> image, string path) : this(image)
        {
            this.ImagePath = path;
        }
    }
}
