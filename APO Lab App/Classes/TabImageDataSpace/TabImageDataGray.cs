﻿using APO_Lab_App.Classes.Converter;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;

namespace APO_Lab_App.Classes.TabImageDataSpace
{
    /// <summary>
    /// Class with grayscale image data in tab. Inheritance from class TabImageData.
    /// </summary>
    public class TabImageDataGray : TabImageData
    {
        /// <summary>
        /// Reference to GrayImage.
        /// </summary>
        public Image<Gray, Byte> GrayImage { get; private set; }

        /// <summary>
        /// Constructor for RGB image.
        /// </summary>
        /// <param name="image">Image</param>
        public TabImageDataGray(Image<Bgr, Byte> image)
        {
            this.GrayImage = image?.Convert<Gray, Byte>();
            SetBitmap(image.ToBitmap());
        }

        /// <summary>
        /// Constructor for grayscale image.
        /// </summary>
        /// <param name="image"></param>
        public TabImageDataGray(Image<Gray, Byte> image)
        {
            this.GrayImage = image;
            SetBitmap(image.ToBitmap());
        }

        /// <summary>
        /// Constructor for loaded from file image.
        /// </summary>
        /// <param name="image">Image</param>
        /// <param name="path">Path</param>
        public TabImageDataGray(Image<Bgr, Byte> image, string path) : this(image)
        {
            this.ImagePath = path;
        }

        private void SetBitmap(Bitmap bitmap)
        {
            this.Bitmap = bitmap;
            this.BitmapImage = BitmapConverter.BitmapToBitmapImageConvert(Bitmap);
        }
    }
}
