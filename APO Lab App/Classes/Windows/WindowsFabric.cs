﻿using APO_Lab_App.Windows;
using System.Collections.Generic;
using System.Windows;

namespace APO_Lab_App.Classes.Windows
{
    /// <summary>
    /// Class with fabric for additional windows in application which will be show by WindowsManager.
    /// </summary>
    public abstract class WindowsFabric
    {
        /// <summary>
        /// Class with information about data in current window.
        /// </summary>
        public class WindowData
        {
            /// <summary>
            /// Reference to base class Window.
            /// </summary>
            public Window Window { get; private set; }

            /// <summary>
            /// True - user can open many this windows, false - no.
            /// </summary>
            public bool ManyOpenings { get; private set; }

            /// <summary>
            /// True - Window is open, false - no open.
            /// </summary>
            public bool IsOpen { get; set; }

            public WindowData(Window window, bool manyOpenings, bool isOpen)
            {
                this.Window = window;
                this.ManyOpenings = manyOpenings;
                this.IsOpen = isOpen;
            }
        }

        /// <summary>
        /// Dictionary with all additional windows in application.
        /// </summary>
        public static readonly Dictionary<string, WindowData> windows = new Dictionary<string, WindowData>()
        {
           { Constants.CONST_PLOT_PROFILE_WINDOW_KEY, new WindowData(new PlotProfile(Constants.CONST_PLOT_PROFILE_WINDOW_KEY), true, false) },
           { Constants.CONST_THRESHOLD_WINDOW_KEY, new WindowData(new ThresholdWindow(Constants.CONST_THRESHOLD_WINDOW_KEY), false, false) },
           { Constants.CONST_THRESHOLD_GRAY_LEVEL_WINDOW_KEY, new WindowData(new ThresholdGrayLevelWindow(Constants.CONST_THRESHOLD_GRAY_LEVEL_WINDOW_KEY), false, false) },
           { Constants.CONST_POSTERIZE_WINDOW_KEY, new WindowData(new PosterizeWindow(Constants.CONST_POSTERIZE_WINDOW_KEY), false, false) },
           { Constants.CONST_STRETCH_RANGE_WINDOW_KEY, new WindowData(new StretchRangeWindow(Constants.CONST_STRETCH_RANGE_WINDOW_KEY), false, false) },
           { Constants.CONST_UNIVERSAL_LINEAR_OPERATIONS_WINDOW, new WindowData(new UniversalLinearOperationWindow(Constants.CONST_UNIVERSAL_LINEAR_OPERATIONS_WINDOW), false, false) },
           { Constants.CONST_ADD_IMAGES_WINDOW, new WindowData(new AddImagesWindow(Constants.CONST_ADD_IMAGES_WINDOW), false, false) },
           { Constants.CONST_BLENDING_WINDOW, new WindowData(new BlendingWindow(Constants.CONST_BLENDING_WINDOW), false, false) },
           { Constants.CONST_LOGIC_OPERATIONS_WINDOW, new WindowData(new LogicOperationsWindow(Constants.CONST_LOGIC_OPERATIONS_WINDOW), false, false) },
           { Constants.CONST_CHANGE_LANGUAGE_WINDOW, new WindowData(new ChangeLanguageWindow(Constants.CONST_CHANGE_LANGUAGE_WINDOW), false, false) },
           { Constants.CONST_FILTER_TWO_STAGERS_WINDOW, new WindowData(new FilterTwoStageWindow(Constants.CONST_FILTER_TWO_STAGERS_WINDOW), false, false) },
           { Constants.CONST_MEDIAN_FILTER_WINDOW, new WindowData(new MedianFilterWindow(Constants.CONST_MEDIAN_FILTER_WINDOW), false, false) },
        };

    }
}
