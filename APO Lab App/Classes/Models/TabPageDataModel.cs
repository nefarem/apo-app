﻿using APO_Lab_App.Classes.Histogram;
using APO_Lab_App.Classes.TabImageDataSpace;
using LiveChartsCore;
using LiveChartsCore.SkiaSharpView;
using LiveChartsCore.SkiaSharpView.Painting;
using SkiaSharp;

namespace APO_Lab_App.Classes.Models
{
    /// <summary>
    /// Class with information which are in tab item.
    /// </summary>
    public class TabPageDataModel
    {
        /// <summary>
        /// Unique page ID.
        /// </summary>
        public int PageID { get; private set; }

        /// <summary>
        /// Reference to class TabImageData.
        /// </summary>
        public TabImageData TabImageData { get; private set; }

        /// <summary>
        /// Reference to class Histogram.
        /// </summary>
        public Histogram.Histogram Histogram { get; private set; }

        /// <summary>
        /// Series with information needed to draw histogram.
        /// </summary>
        public ISeries[] Series { get; private set; }
        
        /// <summary>
        /// File name.
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Current image is grayscale image.
        /// </summary>
        public bool GrayImage { get; private set; }

        /// <summary>
        /// Constructor which create DataModel for grayscale image.
        /// </summary>
        /// <param name="bitmapImage">Bitmap image</param>
        /// <param name="histogramGrayscale">Histogram grayscale object</param>
        /// <param name="fileName">Image file name</param>
        public TabPageDataModel(int pageID, TabImageData tabImageData, HistogramGrayscale histogramGrayscale, string fileName) : this(tabImageData, pageID, fileName)
        {
            this.Histogram = histogramGrayscale;
            this.GrayImage = true;

            this.Series = new ISeries[]
            {
                new ColumnSeries<int>
                {
                    Values = histogramGrayscale.HistogramGrayscaleData.Frequency,
                    Stroke = new SolidColorPaint(SKColors.Black)
                },
            };
        }

        /// <summary>
        /// Constructor which create DataModel for RGB image.
        /// </summary>
        /// <param name="bitmapImage">Bitmap image</param>
        /// <param name="histogramGrayscale">Histogram RGB object</param>
        /// <param name="fileName">Image file name</param>
        public TabPageDataModel(int pageID, TabImageData tabImageData, HistogramRGB histogramRGB, string fileName) : this(tabImageData, pageID, fileName)
        {
            this.Histogram = histogramRGB;
            this.GrayImage = false;

            this.Series = new ISeries[]
            {
                new ColumnSeries<int>
                {
                    Values = histogramRGB.HistogramRGBData.Red,
                    Stroke = new SolidColorPaint(SKColors.Red)
                },

                new ColumnSeries<int>
                {
                    Values = histogramRGB.HistogramRGBData.Green,
                    Stroke = new SolidColorPaint(SKColors.Green)
                },

                new ColumnSeries<int>
                {
                    Values = histogramRGB.HistogramRGBData.Blue,
                    Stroke = new SolidColorPaint(SKColors.Blue)
                }
            };
        }

        /// <summary>
        /// Private constructor is base for two public constructors.
        /// </summary>
        /// <param name="tabImageData">TabImageData</param>
        /// <param name="pageID">PageID</param>
        /// <param name="fileName">FileName</param>
        private TabPageDataModel(TabImageData tabImageData, int pageID, string fileName)
        {
            this.TabImageData = tabImageData;
            this.PageID = pageID;
            this.FileName = fileName;
        }
    }
}
