﻿using APO_Lab_App.Classes.Models;
using System.Collections.ObjectModel;

namespace APO_Lab_App.Classes.ViewModels
{
    /// <summary>
    /// Class with view model for tabs with images.
    /// </summary>
    public class TabPageViewModel : BaseViewModel
    {
        /// <summary>
        /// Observable changes in collection with images.
        /// </summary>
        public ObservableCollection<TabPageDataModel> TabPageDatas
        {
            get => tabPageDatas;
            set
            {
                tabPageDatas = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<TabPageDataModel> tabPageDatas;

        /// <summary>
        /// Constructor.
        /// </summary>
        public TabPageViewModel()
        {
            tabPageDatas = new ObservableCollection<TabPageDataModel>();
        }
    }
}
