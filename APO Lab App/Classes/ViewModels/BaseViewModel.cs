﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace APO_Lab_App.Classes.ViewModels
{
    /// <summary>
    /// Base class for all ViewModel classes with implement PropertyChanged event handler.
    /// </summary>
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
