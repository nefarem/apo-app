﻿using System.Collections.Generic;
using System.Linq;

namespace APO_Lab_App.Classes.Language
{
    /// <summary>
    /// Class with all language codes supported in this application.
    /// </summary>
    public abstract class LanguageFabric
    {
        /// <summary>
        /// Dictionary with language codes.
        /// </summary>
        public static readonly Dictionary<string, string> languages = new Dictionary<string, string>()
        {
           { "pl-PL", "Polski"  },
           { "en-US", "English" }
        };

        /// <summary>
        /// Get all language keys from dictionary languages.
        /// </summary>
        /// <returns>Dictionary keys</returns>
        public static List<string> GetLanguageKeys() => languages.Keys.ToList();

        /// <summary>
        /// Get language key by value.
        /// </summary>
        /// <param name="languageValue">Language value</param>
        /// <returns>Language key</returns>
        public static string GetLanguageKey(string languageValue) => languages.First(x => x.Value == languageValue).Key;
    }
}
