﻿using System.Collections.Generic;
using System.Linq;
using static APO_Lab_App.Classes.Windows.WindowsFabric;

namespace APO_Lab_App.Classes.Managers
{
    /// <summary>
    /// Class manager with all additional windows except MainWindow.
    /// </summary>
    public class WindowsManager
    {
        private List<string> openedWindows;

        public WindowsManager()
        {
            openedWindows = new List<string>();
        }

        /// <summary>
        /// Open window.
        /// </summary>
        /// <param name="windowNameKey">Window name key</param>
        /// <param name="showDialog">True if open window as dialog (block MainWindow), else false. False is default value.</param>
        public void OpenWindow(string windowNameKey, bool showDialog = false)
        {
            WindowData windowData = GetWindowData(windowNameKey);

            if (windowData == null) return;
            if (openedWindows.Any(x => x == windowNameKey) && windowData.ManyOpenings == false) return;

            openedWindows.Add(windowNameKey);

            if (showDialog) windowData.Window.ShowDialog();
            else windowData.Window.Show();    

            windowData.IsOpen = true;
        }

        /// <summary>
        /// Close window.
        /// </summary>
        /// <param name="windowNameKey">Window name key</param>
        public void CloseWindow(string windowNameKey)
        {
           openedWindows.FindAll(x => x.Equals(windowNameKey)).ToList().ForEach(x => GetWindowData(x).IsOpen = false);
           openedWindows.RemoveAll(x => x.Equals(windowNameKey));
        }

        /// <summary>
        /// Get WindowData object.
        /// </summary>
        /// <param name="windowNameKey">Window name key</param>
        /// <returns>WindowData object if dictionary contain key, else return null.</returns>
        public WindowData GetWindowData(string windowNameKey)
        {
            if (windows == null || !windows.ContainsKey(windowNameKey)) return null;

            return windows[windowNameKey];
        }
    }
}
