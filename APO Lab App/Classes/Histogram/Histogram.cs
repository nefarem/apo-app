﻿using System.Drawing;

namespace APO_Lab_App.Classes.Histogram
{
    /// <summary>
    /// Abstract base class for histograms which inheritance by this class.
    /// </summary>
    public abstract class Histogram
    {
        /// <summary>
        /// Histogram bitmap on the basis of which it was draw.
        /// </summary>
        public Bitmap Bitmap => bitmap;
        protected Bitmap bitmap;

        /// <summary>
        /// Histogram base constructor.
        /// </summary>
        /// <param name="bitmap">Bitmap</param>
        public Histogram(Bitmap bitmap)
        {
            this.bitmap = bitmap;

            CreateHistogramData();
        }
         
        /// <summary>
        /// Abstract method to create histogram data.
        /// </summary>
        protected abstract void CreateHistogramData();
    }
}
