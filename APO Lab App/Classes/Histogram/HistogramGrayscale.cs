﻿using System.Drawing;

namespace APO_Lab_App.Classes.Histogram
{
    /// <summary>
    /// Class for grayscale histogram inheritance from histogram.
    /// </summary>
    public class HistogramGrayscale : Histogram
    {
        /// <summary>
        /// Reference to histogram data with information about frequences in image.
        /// </summary>
        public HistogramGrayscaleData HistogramGrayscaleData { get; private set; }

        public HistogramGrayscale(Bitmap bitmap) : base(bitmap)
        {
        }

        /// <summary>
        /// Create histogram data for grayscale image.
        /// </summary>
        protected override void CreateHistogramData()
        {
            int[] frequency = new int[256];

            for (int x = 0; x < bitmap.Width; ++x)
            {
                for (int y = 0; y < bitmap.Height; ++y)
                {
                    int pixelValue = bitmap.GetPixel(x, y).R;
                    frequency[pixelValue]++;
                }
            }

            HistogramGrayscaleData = new HistogramGrayscaleData(frequency);
        }
    }
}
