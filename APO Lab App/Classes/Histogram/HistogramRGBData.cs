﻿namespace APO_Lab_App.Classes.Histogram
{
    /// <summary>
    /// Class with information about RGB values in RGB image.
    /// </summary>
    public class HistogramRGBData
    {
        /// <summary>
        /// Array int with red values.
        /// </summary>
        public int[] Red { get; private set; }

        /// <summary>
        /// Array int with green values.
        /// </summary>
        public int[] Green { get; private set; }

        /// <summary>
        /// Array int with blue values.
        /// </summary>
        public int[] Blue { get; private set; }

        public HistogramRGBData(int[] red, int[] green, int[] blue)
        {
            this.Red = red;
            this.Green = green;
            this.Blue = blue;
        }
    }
}
