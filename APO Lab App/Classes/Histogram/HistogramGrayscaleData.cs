﻿namespace APO_Lab_App.Classes.Histogram
{
    /// <summary>
    /// Class with information about frequences in grayscale image.
    /// </summary>
    public class HistogramGrayscaleData
    {
        /// <summary>
        /// Int array, frequences in grayscale image.
        /// </summary>
        public int[] Frequency { get; private set; }

        public HistogramGrayscaleData(int[] frequency)
        {
            this.Frequency = frequency;
        }
    }
}
