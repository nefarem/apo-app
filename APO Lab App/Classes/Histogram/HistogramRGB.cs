﻿using System.Drawing;

namespace APO_Lab_App.Classes.Histogram
{
    /// <summary>
    /// Class for RGB histogram inheritance from histogram.
    /// </summary>
    public class HistogramRGB : Histogram
    {
        /// <summary>
        /// Reference to histogram data with information about RGB in image.
        /// </summary>
        public HistogramRGBData HistogramRGBData { get; private set; }

        public HistogramRGB(Bitmap bitmap) : base(bitmap)
        {
        }

        /// <summary>
        /// Create histogram data for RGB image.
        /// </summary>
        protected override void CreateHistogramData()
        {
            int[] red = new int[256];
            int[] green = new int[256];
            int[] blue = new int[256];

            for (int x = 0; x < bitmap.Width; ++x)
            {
                for (int y = 0; y < bitmap.Height; ++y)
                {
                    Color pixel = bitmap.GetPixel(x, y);

                    red[pixel.R]++;
                    green[pixel.G]++;
                    blue[pixel.B]++;
                }
            }

            HistogramRGBData = new HistogramRGBData(red, green, blue);
        }
    }
}
