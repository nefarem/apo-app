﻿using System.Drawing;

namespace APO_Lab_App.Classes
{
    /// <summary>
    /// Static class with all constants in project.
    /// </summary>
    public static class Constants
    {
        public static readonly Size CONST_KERNEL_SIZE = new Size(5, 5);

        //public const double CONST_RED_VALUE = 0.2126;
        //public const double CONST_GREEN_VALUE = 0.7152;
        //public const double CONST_BLUE_VALUE = 0.0722;

        public const int CONST_THRESHOLD_VALUE = 127;
        public const int CONST_POSTERIZE_BINS_NUM = 8;

        public const string CONST_PLOT_PROFILE_WINDOW_KEY = "PlotProfile";
        public const string CONST_THRESHOLD_WINDOW_KEY = "ThresholdWindow";
        public const string CONST_THRESHOLD_GRAY_LEVEL_WINDOW_KEY = "ThresholdGrayLevelWindow";
        public const string CONST_POSTERIZE_WINDOW_KEY = "PosterizeWindow";
        public const string CONST_STRETCH_RANGE_WINDOW_KEY = "StretchRangeWindow";
        public const string CONST_UNIVERSAL_LINEAR_OPERATIONS_WINDOW = "UniversalLinearOperationWindow";
        public const string CONST_ADD_IMAGES_WINDOW = "AddImagesWindow";
        public const string CONST_BLENDING_WINDOW = "BlendingWindow";
        public const string CONST_LOGIC_OPERATIONS_WINDOW = "LogicOperationsWindow";
        public const string CONST_CHANGE_LANGUAGE_WINDOW = "ChangeLanguageWindow";
        public const string CONST_FILTER_TWO_STAGERS_WINDOW = "FilterTwoStageWindow";
        public const string CONST_MEDIAN_FILTER_WINDOW = "MedianFilterWindow";
    }
}
