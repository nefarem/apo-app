﻿using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace APO_Lab_App.Classes.Converter
{

    /// <summary>
    /// Static class with methods to convert bitmap format.
    /// </summary>
    public static class BitmapConverter
    {
        /// <summary>
        /// Takes a bitmap and converts it to an image that can be handled by WPF ImageBrush
        /// </summary>
        /// <param name="src">A bitmap image</param>
        /// <returns>The image as a BitmapImage for WPF</returns>
        public static BitmapImage BitmapToBitmapImageConvert(Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            ((Bitmap)bitmap).Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            ms.Seek(0, SeekOrigin.Begin);
            image.StreamSource = ms;
            image.EndInit();
            return image;
        }
    }
}
