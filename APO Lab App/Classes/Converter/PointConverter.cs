﻿using System;

namespace APO_Lab_App.Classes.Converter
{

    /// <summary>
    /// Static class with methods to convert Point from System.Windows.Poin namespace to System.Drawing.Point and in second way.
    /// </summary>
    public static class PointConverter
    {

        /// <summary>
        /// Convert Point from System.Drawing namespace to Point in System.Windows.
        /// </summary>
        /// <param name="point">System.Drawing.Point object</param>
        /// <returns></returns>
        public static System.Windows.Point DrawingPointToWindowsPoint(System.Drawing.Point point)
        {
            return new System.Windows.Point(point.X, point.Y);
        }

        /// <summary>
        /// Convert Point from System.Windows namespace to Point in System.Drawing.
        /// </summary>
        /// <param name="point">System.Windows.Point object</param>
        /// <returns></returns>
        public static System.Drawing.Point WindowsPointToDrawingPoint(System.Windows.Point point)
        {
            return new System.Drawing.Point(Convert.ToInt32(point.X), Convert.ToInt32(point.Y));
        }
    }
}
