﻿using APO_Lab_App.Classes.Histogram;
using APO_Lab_App.Classes.ImageProcessing;
using NUnit.Framework;
using System;
using System.Drawing;
using System.IO;

namespace APO_Lab_App.Tests
{
    internal class ImageProcessingTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void StretchHistogramTest()
        {
            // Act
            string workingDirectory = Environment.CurrentDirectory;
            string resourcesDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName + "\\Resources";

            Bitmap bitmapToProcess = new Bitmap(resourcesDirectory + "\\ZwezonyHistogram.jpg");
            Bitmap bitmapToCompare = new Bitmap(resourcesDirectory + "\\ZwezonyHistogramResult.bmp");

            HistogramGrayscale bitmapToCompareHistogram = new HistogramGrayscale(bitmapToCompare);

            // Arrange
            Bitmap bitmapAfterProcess = ImageProcessing.StretchHistogram(bitmapToProcess);
            HistogramGrayscale bitmapAfterProcessHistogram = new HistogramGrayscale(bitmapToCompare);

            // Assert
            Assert.AreEqual(bitmapAfterProcessHistogram.HistogramGrayscaleData.Frequency, bitmapToCompareHistogram.HistogramGrayscaleData.Frequency);;
            Assert.That(ImageProcessing.GetAllPixelsFromImage(bitmapAfterProcess), Is.EqualTo(ImageProcessing.GetAllPixelsFromImage(bitmapToCompare)).Within(30d));
        }

        [Test]
        public void EqualizeHistogramTest()
        {
            // Act
            string workingDirectory = Environment.CurrentDirectory;
            string resourcesDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName + "\\Resources";

            Bitmap bitmapToProcess = new Bitmap(resourcesDirectory + "\\ZwezonyHistogram.jpg");
            Bitmap bitmapToCompare = new Bitmap(resourcesDirectory + "\\ZwezonyHistogramResult.bmp");

            HistogramGrayscale bitmapToCompareHistogram = new HistogramGrayscale(bitmapToCompare);

            // Arrange
            Bitmap bitmapAfterProcess = ImageProcessing.EqualizeHistogram(bitmapToProcess, new HistogramGrayscale(bitmapToProcess).HistogramGrayscaleData);
            HistogramGrayscale bitmapAfterProcessHistogram = new HistogramGrayscale(bitmapToCompare);

            // Assert
            Assert.AreEqual(bitmapAfterProcessHistogram.HistogramGrayscaleData.Frequency, bitmapToCompareHistogram.HistogramGrayscaleData.Frequency);
            Assert.That(ImageProcessing.GetAllPixelsFromImage(bitmapAfterProcess), Is.EqualTo(ImageProcessing.GetAllPixelsFromImage(bitmapToCompare)).Within(30d));

        }

        [Test]
        public void NegationTest()
        {
            // Act
            string workingDirectory = Environment.CurrentDirectory;
            string resourcesDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName + "\\Resources";

            Bitmap bitmapToProcess = new Bitmap(resourcesDirectory + "\\LenaGray.bmp");
            Bitmap bitmapToCompare = new Bitmap(resourcesDirectory + "\\LenaGrayNegationResult.bmp");

            HistogramGrayscale bitmapToCompareHistogram = new HistogramGrayscale(bitmapToCompare);

            // Arrange
            Bitmap bitmapAfterProcess = ImageProcessing.Negation(bitmapToProcess);
            HistogramGrayscale bitmapAfterProcessHistogram = new HistogramGrayscale(bitmapToCompare);

            // Assert
            Assert.AreEqual(bitmapAfterProcessHistogram.HistogramGrayscaleData.Frequency, bitmapToCompareHistogram.HistogramGrayscaleData.Frequency);
            Assert.That(ImageProcessing.GetAllPixelsFromImage(bitmapAfterProcess), Is.EqualTo(ImageProcessing.GetAllPixelsFromImage(bitmapToCompare)).Within(2d));
        }

        [Test]
        public void ThresholdingTest()
        {
            // Act
            string workingDirectory = Environment.CurrentDirectory;
            string resourcesDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName + "\\Resources";

            Bitmap bitmapToProcess = new Bitmap(resourcesDirectory + "\\LenaGray.bmp");
            Bitmap bitmapToCompare = new Bitmap(resourcesDirectory + "\\LenaGrayThresholdResult.bmp");

            HistogramGrayscale bitmapToCompareHistogram = new HistogramGrayscale(bitmapToCompare);

            // Arrange
            Bitmap bitmapAfterProcess = ImageProcessing.Thresholding(bitmapToProcess, 147);
            HistogramGrayscale bitmapAfterProcessHistogram = new HistogramGrayscale(bitmapToCompare);

            // Assert
            Assert.AreEqual(bitmapAfterProcessHistogram.HistogramGrayscaleData.Frequency, bitmapToCompareHistogram.HistogramGrayscaleData.Frequency);
            Assert.That(ImageProcessing.GetAllPixelsFromImage(bitmapAfterProcess), Is.EqualTo(ImageProcessing.GetAllPixelsFromImage(bitmapToCompare)).Within(2d));
        }

        [Test]
        public void PosterizeTest()
        {
            // Act
            string workingDirectory = Environment.CurrentDirectory;
            string resourcesDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName + "\\Resources";

            Bitmap bitmapToProcess = new Bitmap(resourcesDirectory + "\\LenaGray.bmp");
            Bitmap bitmapToCompare = new Bitmap(resourcesDirectory + "\\LenaGrayPosterizeResult.bmp");

            HistogramGrayscale bitmapToCompareHistogram = new HistogramGrayscale(bitmapToCompare);

            // Arrange
            Bitmap bitmapAfterProcess = ImageProcessing.Posterize(bitmapToProcess, 8);
            HistogramGrayscale bitmapAfterProcessHistogram = new HistogramGrayscale(bitmapToCompare);

            // Assert
            Assert.AreEqual(bitmapAfterProcessHistogram.HistogramGrayscaleData.Frequency, bitmapToCompareHistogram.HistogramGrayscaleData.Frequency);
            Assert.That(ImageProcessing.GetAllPixelsFromImage(bitmapAfterProcess), Is.EqualTo(ImageProcessing.GetAllPixelsFromImage(bitmapToCompare)).Within(50d));
        }

    }
}
